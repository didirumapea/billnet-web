export const vueI18n = {"fallbackLocale":"id"}
export const vueI18nLoader = false
export const locales = [{"code":"ar","iso":"ar-AR","name":"Arabic","file":"ar-AR.js","dir":"rtl"},{"code":"de","iso":"de-DE","name":"Deutch","file":"de-DE.js","dir":"ltr"},{"code":"en","iso":"en-US","name":"English","file":"en-US.js","dir":"ltr"},{"code":"id","iso":"id-ID","name":"Bahasa Indonesia","file":"id-ID.js","dir":"ltr"},{"code":"pt","iso":"pt-PT","name":"Portuguese","file":"pt-PT.js","dir":"ltr"},{"code":"zh","iso":"zh-ZH","name":"Chinese","file":"zh-ZH.js","dir":"ltr"}]
export const defaultLocale = "id"
export const defaultDirection = "ltr"
export const routesNameSeparator = "___"
export const defaultLocaleRouteNameSuffix = "default"
export const strategy = "prefix_except_default"
export const lazy = true
export const langDir = "/Users/didi-rim-01/Didi/node-js/frontend/billnet-web/static/lang"
export const rootRedirect = null
export const detectBrowserLanguage = {"useCookie":true,"cookieCrossOrigin":false,"cookieDomain":null,"cookieKey":"i18n_redirected","cookieSecure":false,"alwaysRedirect":false,"fallbackLocale":"","onlyOnNoPrefix":false,"onlyOnRoot":false}
export const differentDomains = false
export const seo = false
export const baseUrl = ""
export const vuex = {"moduleName":"i18n","syncLocale":false,"syncMessages":false,"syncRouteParams":true}
export const parsePages = true
export const pages = {}
export const skipSettingLocaleOnNavigate = false
export const beforeLanguageSwitch = () => null
export const onLanguageSwitched = () => null
export const IS_UNIVERSAL_MODE = true
export const MODULE_NAME = "nuxt-i18n"
export const LOCALE_CODE_KEY = "code"
export const LOCALE_ISO_KEY = "iso"
export const LOCALE_DIR_KEY = "dir"
export const LOCALE_DOMAIN_KEY = "domain"
export const LOCALE_FILE_KEY = "file"
export const STRATEGIES = {"PREFIX":"prefix","PREFIX_EXCEPT_DEFAULT":"prefix_except_default","PREFIX_AND_DEFAULT":"prefix_and_default","NO_PREFIX":"no_prefix"}
export const COMPONENT_OPTIONS_KEY = "nuxtI18n"
export const localeCodes = ["ar","de","en","id","pt","zh"]
export const trailingSlash = undefined

export const ASYNC_LOCALES = {
  'ar-AR.js': () => import('../../static/lang/ar-AR.js' /* webpackChunkName: "lang-ar-AR.js" */),
  'de-DE.js': () => import('../../static/lang/de-DE.js' /* webpackChunkName: "lang-de-DE.js" */),
  'en-US.js': () => import('../../static/lang/en-US.js' /* webpackChunkName: "lang-en-US.js" */),
  'id-ID.js': () => import('../../static/lang/id-ID.js' /* webpackChunkName: "lang-id-ID.js" */),
  'pt-PT.js': () => import('../../static/lang/pt-PT.js' /* webpackChunkName: "lang-pt-PT.js" */),
  'zh-ZH.js': () => import('../../static/lang/zh-ZH.js' /* webpackChunkName: "lang-zh-ZH.js" */)
}
