const brand = {
  agency: {
    name: 'Billnet Mitracom',
    desc: 'Billnet Mitracom - Printing & Mailing Service',
    prefix: 'billnet',
    footerText: 'Billnet - All Rights Reserved 2021',
    logoText: 'Billnet Mitracom',
    projectName: 'Billnet',
    url: 'veluxi.ux-maestro.com/agency',
    img: '/static/images/agency-logo.png',
    notifMsg:
      'Donec sit amet nulla sed arcu pulvinar ultricies commodo id ligula.'
  }
}

export default brand
