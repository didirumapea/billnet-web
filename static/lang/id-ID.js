export default () => {
  return new Promise(function(resolve) {
    resolve({
      common: {
        title: 'Contoh sederhana',
        subtitle: 'Ini halaman kosong. Mulai buat halaman Anda dari sini.',
        error_with_status: 'Terjadi kesalahan di server',
        '404': 'Halaman ini tidak dapat ditemukan.',
        '404_subtitle':
          'Anda bisa tinggal dan bersantai di sini, atau kembali ke awal.',
        back: 'kembali ke beranda',
        error_without_status: 'Terjadi kesalahan di server',
        contact_title: 'Kutipan Proyek Gratis',
        contact_subtitle:
          'Apakah Anda memiliki pertanyaan? Apakah Anda siap untuk mengurangi biaya dan membuat pendapatan baru? Atau Anda hanya ingin menyapa? Kirim pesan kepada kami.',
        form_name: 'Siapa nama Anda? *',
        form_email: 'Apa itu email Anda? *',
        form_phone: 'Berapa nomor telepon Anda?',
        form_company: 'Apa perusahaan Anda?',
        form_message: 'Tulis pesan Anda di sini',
        form_terms: 'Saya telah membaca dan menerima Ketentuan',
        form_privacy: 'Layanan & Kebijakan Privasi *',
        form_send: 'Kirim Pesan',
        accept: 'Setuju',
        en: 'English',
        de: 'Deutsch',
        zh: '简体中文',
        pt: '󠁥󠁮󠁧󠁿Português',
        id: '󠁥󠁮󠁧󠁿Bahasa Indonesia',
        ar: '󠁥󠁮󠁧󠁿العربيّة',
        notif_msg: 'Kami menggunakan cookie untuk memastikan bahwa kami memberi Anda pengalaman terbaik di situs web kami. Dengan menutup pesan ini, Anda menyetujui cookie kami di perangkat ini sesuai dengan kebijakan cookie kami kecuali Anda telah menonaktifkannya.'
      },
      agencyLanding: {
        header_about: 'tentang',
        header_services: 'layanan',
        'header_our-expertise': 'keahlian kami',
        header_testimonials: 'testimoni',
        'header_case-studies': 'studi kasus',
        header_our_office: 'kantor kame',
        header_contact: 'kontak',
        header_language: 'bahasa',
        header_theme: 'mode tema',
        header_dark: 'gelap',
        header_light: 'terang',
        banner_title: 'Billnet Mitracom',
        banner_subtitle:
          'Merupakan perusahaan Printing & Mailing Service, yang didesain khusus untuk menangani pekerjaan pencetakan, pengamplopan dan pengiriman dokumen.',
        banner_button: 'menulis kepada kami',
        about_title: 'Tentang kami',
        about_subtitle:
          'Melalui kapasitas besar kami, kami berani mengatakan bahwa kami berbeda.',
        about_employee: 'printing',
        about_projects: 'enveloping',
        about_client: 'klien',
        about_quote: 'Keamanan data dan kecepatan pengolahan adalah prioritas kami.',
        services_title: 'Layanan Kami',
        services_button: 'lihat detail',
        expertise_title: 'Keahlian Kami',
        expertise_subtitle:
          'Proyek yang sukses dibangun dengan tim yang sukses.',
        expertise_paragraph:
          'Pelayanan yang diberikan PT. Billnet Mitracom tidak hanya terbatas pada form billing, kami menyediakan pelayanan dalam segala hal yang berhubungan dengan pengolahan data dan pencetakan massal dengan tepat waktu dan fleksibel sesuai kebutuhan, meliputi :',
        testimonial_title: 'testimonial klien',
        case_title: 'Studi kasus',
        cta_title: 'Siap memulai?',
        cta_subtitle:
          'Tim Teknologi Terbaik tidak terlahir instan, mereka dibuat ...',
        cta_btn: 'hubungi kami',
        office_title: 'Kantor Kami',
        office_head: 'Kantor Pusat',
        office_branch: 'Kantor Cabang',
        footer_paragraph:
          'Kami telah menjalankan bisnis selama hampir satu dekade. Membantu Anda adalah prioritas utama kami.'
      }
    })
  })
}
