module.exports = {
  apps : [
    {
      name: "billnet-web-dev",
      script: "npm",
      args: "run dev"
    },
    {
      name: "billnet-web-prod",
      script: "npm",
      args: "run start"
    },
    {
      name: "billnet-web-staging",
      script: "npm",
      args: "run start"
    }
  ]
}
